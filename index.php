<html>
<head>
  <title>Project</title>
  <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="font-awesome.css">
</head>
<body>
  <div class="wrapper">


<!--Header Transparent-->


    <nav class="navbar navbar-fixed-top" data-spy="affix" data-offset-top="660">
        <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
        <a class="navbar-brand">WAPIK</a>


      <div style="position: absolute; left: 100px">
      <ul class="nav navbar-nav">
        <li><a >+1234567</a></li>
      </ul></div>

      <div style="position: absolute; left: 770px">
      <ul class="nav navbar-nav">
        <li class="active"><a>HOME</a></li>
        <li><a href="#">ABOUT US</a></li>
        <li><a href="#">PRICING</a></li>
        <li><a href="#">CONTACT US</a></li>
        <li><a href="#" class="btn btn-success my-2 my-sm-0" style="background:#6FB048;color:#fff;padding:8px 8px;margin-top:6px;">Get Started</a></li>
      </ul></div></div>
      </div></div>
    </nav>    </nav>

<!--Banner-->


  <section id="bener" class="img-responsive"  style="background: url(img/banner.jpg)">
 <div class="bener_overlay" >
<div class="container" >
<div class="bener_content">
  <h2>Welcome to my Page</h2><br>
  <h4><p>Welcome to the Wapik Welcome to the WapikWelcome to the WapikWelcome to the Wapik</p></h4>
  <a href="" class="btn btn-success my-2 my-sm-0">See More</a>
  </div>
  </div>
  </div>
  </section>


<!--Features-->

  <section id="features">
  <div class="container">
  <div class="features_top text center">
<center>   <h2>Best of our feature</h2>
  <p>This is new technology and we are looking forward to build a good team for new projects and assignments.</p></center>
  </div>

  <div class="row">

   <div class="col-md-4">
    <center> <img src="img/icon.png">
     <h3>New Icons</h3></center>
     <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem.</p>
   </div>
   <div class="col-md-4">
     <center><img src="img/icon3.png">
     <h3>New Icons</h3>
     <p>Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p></center>
   </div>
   <div class="col-md-4">
     <center><img src="img/icon2.png">
     <h3>New Icons</h3>
     <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna.</p></center>
   </div>
 </div>
 <br><br><br><br><br><br>


 <div class="border_img clear"></div>
 <!-- Best Features Bottom -->
 <div class="best_features_bottom">
     <div class="col-md-6 col-sm-6">
         <div class="single_features_bottom">

             <h2><img src="img/icon4.png" alt='' /><i class="icon icon-global"></i>Cross Browser Compatibility</h2>
             <p>Donec ullamcorper nulla non metus auctor fringilla. Donec id elit non mi porta gravida at eget metus.</p>
         </div>
     </div>

     <div class="col-md-6 col-sm-6">
         <div class="single_features_bottom">
             <h2><img src="img/icon5.png" alt='' /><i class="icon icon-lightbulb"></i> Creative Idea</h2>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula porta felis euismod semper.</p>
         </div>
     </div>

     <div class="col-md-6 col-sm-6">
         <div class="single_features_bottom">
           <h2><img src="img/icon6.png" alt='' /><i class="lnr lnr-magic-wand"></i> Pixel Perfect</h2>
             <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
         </div>
     </div>

     <div class="col-md-6 col-sm-6">
         <div class="single_features_bottom">
             <h2><img src="img/icon7.png" alt='' /><i class="lnr lnr-cloud-download"></i> Free Forever and Ever</h2>
             <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec id elit non mi porta gravida at eget metus.</p>
         </div>
     </div>

 </div><!-- End Best Features Bottom -->

</div>
</div>

  </section>


  <!--Looking Section-->

  <section id="new">
    <div class="container">
      <div class="new_top text center">
      <center><h2>Looking for perfect template to use</h2>
      <p>This is new technology and we are looking forward to build a good team for new projects and assignments.</p></center>
      </div>

      <div class="row">
     <div class="col-md-6">
      <center> <img src="img/mobile-screen.png" alt='' /></center>
      </div>

     <div class="col-md-6">
<div class="new_top text center">
<h2>Responsive ready</h2>
<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas sed diam eget risus varius blandit sit amet non magna. Vestibulum id ligula porta felis euismod semper.

Integer posuere erat a ante venenatis dapibus posuere velit aliquet.

Donec id elit non mi porta gravida at eget metus.</p>
     </div></div>
     </div>
    </div>
  </section>

  <section id="neww">
    <div class="container">
    <div class="row">
   <div class="col-md-6">
  <div class="new_top text center">
    <br><br>
<br><br><br>
  <h3>This is new technology and we are looking forward to build a good team for new projects and assignments.</h3>
  </div>  </div>


  <div class="col-md-6">
  <center> <img src="img/test.png"></center>
  </div>
   </div>
 </div>
  </section>

  <section id="new">
    <div class="container">
    <div class="row">
   <div class="col-md-6">
  <div class="new_top text center">
<h2>Built with High Attention to Details</h2>
  <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur. Maecenas sed diam eget risus varius blandit sit amet non magna. Vestibulum id ligula porta felis euismod semper. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec id elit non mi porta gravida at eget metus.
</p>
  </div>  </div>


  <div class="col-md-6">
  <center> <img src="img/map.png" class="img-responsive"></center>
  </div>
   </div>
 </div>
  </section>


  <section id="pricing" class="text-center">
  <div class="container">
  <div class="row">
  <div class="pricing_top">
  <h2>Pricing & Plan</h2>
  <div class="separator"></div>
  <p>This is Spacing Parameter</p>
  </div>

  <div class="pricing_tabel_contant">
  <div class="col-md-offset-3 col-md-2 col-sm-2">
  <div class="single_pricing">
  <div class="top_text">
    <span>Basic</span>
    <sub>$ 199</sub>
    <sup>/month</sup>
    </div>
    <ul>
    <li>One</li>
    <li>Two</li>
  <li>Three</li>
  <li>Four</li>
    </ul>
<a href="" class="btn btn-primary btn-sm">Select Plan</a>
  </div>
  </div>
  <div class=" col-md-2 col-sm-2">
  <div class="single_pricing">
  <div class="top_text">
  <span>Basic</span>
  <sub>$ 399</sub>
  <sup>/month</sup>
    </div>
    <ul>
    <li>One</li>
  <li>Two</li>
<li>Three</li>
  <li>Four</li>
  </ul>
                              <a href="" class="btn btn-primary btn-sm">Select Plan</a>
                          </div>
                      </div>
                      <div class=" col-md-2 col-sm-2">
                          <div class="single_pricing">
                              <div class="top_text">
                                  <span>Basic</span>
                                  <sub>$ 699</sub>
                                  <sup>/month</sup>
                              </div>
                              <ul>
                                  <li>One</li>
                                  <li>Two</li>
                                  <li>Three</li>
                                  <li>Four</li>
                              </ul>
                              <a href="" class="btn btn-primary btn-sm">Select Plan</a>
                          </div>
                      </div>
                  </div>
              </div>




          </div>

      </div>
  </section><!-- End pricing Section -->


  <section id="new">
    <div class="container">
      <div class="new_top text center">
      <center><h2>Included with all plans</h2></center>
      </div>

      <div class="row">
       <div class="col-md-4">
           <div class="new_top text center">
        <center>
          <h4>This is Simple and Easy to learn</h4>
          <h4>This is Simple and Easy to learn</h4>
          <h4>This is Simple and Easy to learn</h4>
    </center>
       </div></div>
       <div class="col-md-4">
         <div class="new_top text center">
           <center>
             <h4>This is Simple and Easy to learn</h4>
             <h4>This is Simple and Easy to learn</h4>
             <h4>This is Simple and Easy to learn</h4>
       </center>
     </div></div>
       <div class="col-md-4">
         <div class="new_top text center">
           <center>
             <h4>This is Simple and Easy to learn</h4>
             <h4>This is Simple and Easy to learn</h4>
             <h4>This is Simple and Easy to learn</h4>
       </center>
     </div>
       </div>
     </div>

    </div>
  </section>


  <section id="neww">
  <div class="container">
  <div class="new_top text center">
<center>  <img src="img/icon8.png"> <h2>Need Help?</h2>
  <p>This is new technology and we are looking forward to build a good team for new projects and assignments.</p>
<h3><b>Support@wapik.com</b></h3></center>
  </div>
</div>
</section>


<section id="top" class=""  style="background: url(img/download-bg.jpg)">
<div class="container" >
  <div class="row">
   <div class="col-md-6">
     <div class="single_like">
     <center><h2>Responsive ready</h2>
     <h3>This is new technology and we are looking forward to build a good team for new projects and assignments.</h3>
   <a href="" class="btn btn-success my-2 my-sm-0">See More</a></center>
     </div>
</div>
 <div class="col-md-6">
   <center><a href="#" class="btn btn-success my-2 my-sm-0" style="background:#6FB048;color:#fff;border-radius:1px;padding:15px 15px;margin-top:135px;">Download Now - Free Forever</a></center></div>
</div>
</div>
</section>


<section id="new">
  <div class="container">
  <div class="row">
     <div class="col-md-3">
         <div class="new_top text center">
      <center> <h2>Wapik</h2>
        <h4>One</h4>
        <h4>Two</h4>
        <h4>Three</h4></center>
     </div></div>
     <div class="col-md-3">
       <div class="new_top text center">
    <center> <h2>Company</h2>
      <h4>One</h4>
      <h4>Two</h4>
      <h4>Three</h4></center>
   </div></div>
     <div class="col-md-3">
       <div class="new_top text center">
    <center> <h2>Others</h2>
      <h4>One</h4>
      <h4>Two</h4>
      <h4>Three</h4></center>
   </div>
     </div>
     <div class="col-md-3">
       <div class="new_top text center">
    <center> <h2>Newsletter</h2>
      <h2><p>Subscribe to our newsletter and get all the cool news</p></h2>
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"><br>
     <a href="" class="btn btn-success my-2 my-sm-0">Send</a>
    </center>
   </div></div>
   </div>

   <div class="footer_bottom">
                  <div class="row">
          <div class="col-md-6">
            <div class="single_footer_bottom">
            <h4> <p>Made with heart by<a href="http://bootstrapthemes.co">Bootstrap Themes</a>2016. All Rights Reserved</p></h4>
            </div>
          </div>
        </div>
              </div>

  </div>
</section>

</div>

<!--Javascript Libraries-->



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
