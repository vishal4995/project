<?php
include 'connection.php';
?>
<html>
<head>
<title>Admin Demo</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="style.css" />
</head>
<body>
  <form name="actionForm" action="delete.php" method="post" onsubmit="return deleteConfirm();"/>

<div class="container box">
<h1 align="center">Admin Dashboard</h1>
<br />
<div class="table-responsive">
<br />
<div align="right">

   <input type="submit" class="btn btn-info btn-lg" name="btn_delete" value="Delete"/>



</div>
<br /><br />
<table id="user_data" class="table table-bordered table-striped">
 <thead>
  <tr>
    <th><input type="checkbox" name="check_all" id="check_all" value=""/></th>

   <th width="20%">Name</th>
   <th width="20%">DOB</th>
   <th width="30%">Address</th>
   <th width="10%">City</th>
   <th width="10%">State</th>
   <th width="10%">Gender</th>
   <th width="10%">Count</th>
   <th width="10%">Operation</th>
  </tr>
 </thead>
 <tbody>
   <?php
   $id= $_GET["id"];
   $query="SELECT * FROM data WHERE id=".$_GET["id"]."";
   $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
   if ($result->num_rows > 0) {
  // output data of each row
  while($row = mysqli_fetch_assoc($result)) {
  echo "<tr>";
  echo "<td>"."<input type='checkbox' name='checkbox[]' id='checkbox[]' value='".$row["id"]."'> " ."</td><td>".$row["name"]."</a></td><td>". $row["dob"]."</td><td>". $row["address"]."</td><td>". $row["city"]."</td><td>". $row["state"]."</td><td>". $row["gender"]
  ."</td><td>". $row["count"]."</td><td>"."<input type='button' id='".$row["id"]."' class='btn btn-primary edit_data' name='edit_data' data-toggle='modal' data-target='#userModal' value='Edit'>"."</td>";
  }
  } else {
  echo "0 results";
  }


  mysqli_close($conn);
  ?>
</tbody>
</table>

</div>
</div>
</form>






<div id="userModal" class="modal fade">
<div class="modal-dialog">
<form method="post" id="user_form" enctype="multipart/form-data" action="wel.php">
<div class="modal-content">
<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal">&times;</button>
 <h4 class="modal-title">Add User</h4>
</div>
<div class="modal-body">
 <label>Name</label>
 <input type="text" name="name" id="name" class="form-control" required>
 <br />
 <label>DOB</label>
 <input type="date" name="dob" id="dob" class="form-control" required/>
 <br />
 <label>Address</label>
 <input type="text" name="addr" id="addr" class="form-control" required/>
 <br />
 <label>City</label>
 <input type="text" name="city" id="city" class="form-control" required/>
 <br />
 <label>State</label>
 <input type="text" name="state" id="state" class="form-control" required/>
 <br />
 <label>Gender</label>
 <input type="text" name="gender" id="gender" class="form-control" required/>
 <br />
  <input type="hidden" name="id" id="id">

</div>
<div class="modal-footer">
 <input type="hidden" name="user_id" id="user_id" />
 <input type="hidden" name="operation" id="operation" />
 <input type="submit" name="submit" id="action" class="btn btn-success" value="add"/>
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</form>

</div>
</div>






<!--child-->




<form action='#' method="post" onclick="delConfirm();">
<div class="header">
  <div align="right">
  <div class="container">
<h1 align="center">Child Dashboard</h1>
 <input type="submit" class="btn btn-info btn-lg" name="btn_delete" value="Delete"/>
<button type='button'  class='btn btn-info btn-lg'  data-toggle='modal' data-target='#userModal1'>Add Child</button>

</div>

<div id="userModal1" class="modal fade">
<div class="modal-dialog">
<form method="post" id="user_form" enctype="multipart/form-data" action="wel.php">
<div class="modal-content">
<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal">&times;</button>
 <h4 class="modal-title"></h4>
</div>
<div class="modal-body">
 <label>User ID</label>
 <input type="text" name="user_id" id="id" class="form-control" required>
 <br />
 <label>Name</label>
 <input type="date" name="name" id="name" class="form-control" required/>
 <br />
 <label>School Name</label>
 <input type="text" name="school_name" id="sname" class="form-control" required/>
 <br />
 <label>Age</label>
 <input type="text" name="age" id="age" class="form-control" required/>
 <br />
 <label>id</label>
 <input type="hidden" name="id" id="id" class="form-control" required/>
 <br />

  <input type="hidden" name="id" id="id">

</div>
<div class="modal-footer">
 <input type="hidden" name="user_id" id="user_id" />
 <input type="hidden" name="operation" id="operation" />
 <input type="submit" name="submit" id="action" class="btn btn-success" value="add"/>
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<div class="table-responsive">
<table id="user_data" class="table table-bordered table-striped">
<thead>
<tr>
  <th width="5%"><input type="checkbox" name="check_all" id="check_all" value=""/></th>

 <th width="20%">Name</th>
 <th width="30%">School Name</th>
 <th width="10%">Age</th>
 <th width="20%">City</th>

</tr>
</thead>
<tbody>
  <?php
  $query="SELECT * FROM child";
  $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
  if ($result->num_rows > 0) {
 // output data of each row
 while($row = mysqli_fetch_assoc($result)) {
 echo "<tr>";
 echo "<td>"."<input type='checkbox' name='checkb[]' id='checkb[]' value='".$row["id"]."'> " ."</td><td>".$row["name"]."</td><td>". $row["sname"]."</td><td>". $row["age"]."</td><td>"."<input type='button' id='".$row["id"]
 ."' class='btn btn-primary edit' name='edit' data-toggle='modal' data-target='#user' value='Edit'>"."</td>";
 }
 } else {
 echo "0 results";
 }


 mysqli_close($conn);
 ?>
</tbody>
</table>

</div>
</form>






<script type="text/javascript">
function delConfirm(){
    var result = confirm("Do you really want to delete records?");
    if(result){
        return true;
    }else{
        return false;
    }
}
$(document).ready(function(){
    $('#check_all').on('click',function(){
        if(this.checked){
            $('.checkb').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkb').each(function(){
                this.checked = false;
            });
        }
    });

    $('.checkb').on('click',function(){
        if($('.checkb:checked').length == $('.checkb').length){
            $('#check_all').prop('checked',true);
        }else{
            $('#check_all').prop('checked',false);
        }
    });
});

$('#add').on('click', function()
    {
        $("#form").trigger("reset");
    });


$(document).on('click', '.edit', function(){
           var id = $(this).attr("id");
          //alert(id);
           $.ajax(
{
                url:"edit2.php",
                method:"POST",
                data:{id:id},
                dataType:"json",
                success:function(data){
                     $('#name').val(data.name);
                    $('#dob').val(data.dob);
                     $('#addr').val(data.address);
                     $('#city').val(data.city);
                    $('#state').val(data.state);
                     $('#gender').val(data.gender);
                     $('#id').val(data.id);
                     $('#act').val("Update");
                     $('#user').modal('show');
                }
           });
      });
</script>


<!--Parents data script-->

<script type="text/javascript">
function deleteConfirm(){
    var result = confirm("Do you really want to delete records?");
    if(result){
        return true;
    }else{
        return false;
    }
}
$(document).ready(function(){
    $('#check_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });

    $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#check_all').prop('checked',true);
        }else{
            $('#check_all').prop('checked',false);
        }
    });
});

$('#add').on('click', function()
    {
        $("#user_form").trigger("reset");
    });


$(document).on('click', '.edit_data', function(){
           var id = $(this).attr("id");
          //alert(id);
           $.ajax(
{
                url:"edit.php",
                method:"POST",
                data:{id:id},
                dataType:"json",
                success:function(data){
                     $('#name').val(data.name);
                    $('#dob').val(data.dob);
                     $('#addr').val(data.address);
                     $('#city').val(data.city);
                    $('#state').val(data.state);
                     $('#gender').val(data.gender);
                     $('#id').val(data.id);
                     $('#action').val("Update");
                     $('#userModal').modal('show');
                }
           });
      });
</script>



</body>
</html>



<!-- Edit Data-->
