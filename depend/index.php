<html>
<head>
</head>
<body>
  <span><select id="continent" name="continent"> <option value="">Select continent</option></select></span>
    <span><select id="country" name="country"> <option value="">Select country</option></select></span>
      <span><select id="state" name="state"> <option value="">Select state</option></select></span>


<!--Script starts-->


    <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){

 load_json_data('continent');

 function load_json_data(id, par)
 {
  var html_code = '';

  //Json load_json_data

  
  $.getJSON('http://localhost/depend/info.php', function(continent){
    html_code += '<option value="">Select '+id+'</option>';
       $.each(continent, function(key, value){
         if(id == 'continent')
            {
             if(value.par == '0')
             {
              html_code += '<option value="'+value.id+'">'+value.name+'</option>';
             }
            }
            else
            {
             if(value.par == par)
             {
              html_code += '<option value="'+value.id+'">'+value.name+'</option>';
             }
            }
           });
           $('#'+id).html(html_code);
          });

         }

         $(document).on('change', '#continent', function(){
          var continent_id = $(this).val();
          if(continent_id != '')
          {
           load_json_data('country', continent_id);
          }
          else
          {
           $('#country').html('<option value="">Select country</option>');
           $('#state').html('<option value="">Select state</option>');
          }
         });
         $(document).on('change', '#country', function(){
          var country_id = $(this).val();
          if(country_id != '')
          {
           load_json_data('state', country_id);
          }
          else
          {
           $('#state').html('<option value="">Select state</option>');
          }
         });
        });
</script>
</body>
</html>
