<?php
$config["base_url"] = base_url();
$config["per_page"] =  10;
$config["total_rows"] = 10;
$config["uri_segment"] = 5;
$choice = $config["total_rows"] / $config["per_page"];
$config["num_links"] = round($choice);
$config['full_tag_open']="<ul class='pagination' style='margin-left:45%;'>";
$config['full_tag_close']="</ul>";
$config['first_tag_open']="<li>";
$config['first_tag_close']="</li>";
$config['last_link_open']="<li>";
$config['last_link_close']="</li>";
$config['next_tag_open']="<li>";
$config['close_tag_close']="</li>";
$config['prev_tag_open']="<li>";
$config['prev_tag_close']="</li>";
$config['num_tag_open']="<li>";
$config['num_tag_close']="</li>";
$config['cur_tag_open']="<li class='active'><a>";
$config['cur_tag_close']="</a></li>";
$config['page_query_string'] = false;
$config['reuse_query_string'] = true;
 ?>
