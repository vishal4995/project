<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Error or success messages
| -------------------------------------------------------------------
*/

$lang['require_parameter'] ='Require parameter missing';
$lang['invalid_parameter']="invalid paramater";
$lang['invalid_access_token']= "invalid access_token";
$lang['access_token_expair']="access token expair";


$lang['block_success']="Block successfully";
$lang['delete_success']="Deleted successfully";
$lang['msg_send']="Message has been send successfully";
$lang['invalid_id']="Invalid id";
$lang['message_list']="Message list";
$lang['no_message']="No more message";


$lang['validation_success']="Password Changed Successfully";

