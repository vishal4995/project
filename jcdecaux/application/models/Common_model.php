<?php

class Common_model extends CI_Model {

    public $finalrole = array();

    public function __construct() {
        $this->load->database();
        $this->load->library('session');
    }

    /**
     * Fetch data from any table based on different conditions
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	array
     * @return	bool
     */
    public function fetch_data($data) {
        //Preparing query
        $condition = "a_email =" . "'" . $data['a_email'] . "' AND " . "a_password =" . "'" . md5($data['a_password']) . "'";
//print_r($condition);die;
        $this->db->select('*');
        $this->db->from('jx_admin');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();

        if ($result['a_status'] == 1) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Insert data in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function insert_single($table, $data = array()) {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }

        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * Insert batch data
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @param	bool
     * @return	bool
     */
    public function insert_batch($table, $defaultArray, $dynamicArray = array(), $updatedTime = false) {
        //Check if default array has values
        if (count($defaultArray) < 1) {
            return false;
        }

        //If updatedTime is true
        if ($updatedTime) {
            $defaultArray['UpdatedTime'] = time();
        }

        //Iterate it
        if (count($dynamicArray)) {
            foreach ($dynamicArray as $val) {
                $updates[] = array_merge($defaultArray, $val);
                return $this->db->insert_batch($table, $updates);
            }
        } else {
            return $this->db->insert_batch($table, $defaultArray);
        }
    }

    /**
     * Delete data from DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function delete_data($table, $conditions = array()) {
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->delete($table);
    }

    /**
     * Handle different conditions of query
     *
     * @access	public
     * @param	array
     * @return	bool
     */
    private function condition_handler($conditions) {
        //Where
        if (array_key_exists('where', $conditions)) {

            //Iterate all where's
            foreach ($conditions['where'] as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        //Where OR
        if (array_key_exists('or_where', $conditions)) {

            //Iterate all where or's
            foreach ($conditions['or_where'] as $key => $val) {
                $this->db->or_where($key, $val);
            }
        }

        //Where In
        if (array_key_exists('where_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_in'] as $key => $val) {
                $this->db->where_in($key, $val);
            }
        }

        //Where Not In
        if (array_key_exists('where_not_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_not_in'] as $key => $val) {
                $this->db->where_not_in($key, $val);
            }
        }

        //Having
        if (array_key_exists('having', $conditions)) {
            $this->db->having($conditions['having']);
        }

        //Group By
        if (array_key_exists('group_by', $conditions)) {
            $this->db->group_by($conditions['group_by']);
        }

        //Order By
        if (array_key_exists('order_by', $conditions)) {

            //Iterate all order by's
            foreach ($conditions['order_by'] as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        //Order By
        if (array_key_exists('like', $conditions)) {

            //Iterate all likes
            foreach ($conditions['like'] as $key => $val) {
                $this->db->like($key, $val);
            }
        }

        //Limit
        if (array_key_exists('limit', $conditions)) {

            //If offset is there too?
            if (count($conditions['limit']) == 1) {
                $this->db->limit($conditions['limit'][0]);
            } else {
                $this->db->limit($conditions['limit'][0], $conditions['limit'][1]);
            }
        }
    }

    /**
     * Update Batch
     *
     * @access	public
     * @param	string
     * @param	array
     * @return	boolean
     */
    public function update_batch_data($table, $defaultArray, $dynamicArray = array(), $key) {
        //Check if any data
        if (count($dynamicArray) < 1) {
            return false;
        }

        //Prepare data for insertion
        foreach ($dynamicArray as $val) {
            $data[] = array_merge($defaultArray, $val);
        }
        return $this->db->update_batch($table, $data, $key);
    }

    /**
     * Update details in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @return	string
     */
    public function update_single($table, $updates, $conditions = array()) {
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->update($table, $updates);
    }

    /**
     * Count all records
     *
     * @access	public
     * @param	string
     * @return	array
     */
    public function fetch_count($table, $conditions = array()) {
        $this->db->from($table);
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->count_all_results();
    }

    /**
     * For sending mail
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	string
     * @param	boolean
     * @return	array
     */
    public function sendmail($email, $subject, $message = false, $single = true, $param = false, $templet = false) {
        if ($single == true) {
            $this->load->library('email');
        }
        $this->config->load('email');
        $this->email->from($this->config->item('from'), $this->config->item('from_name'));
        $this->email->reply_to($this->config->item('repy_to'), $this->config->item('reply_to_name'));
        $this->email->to($email);
        $this->email->subject($subject);
        if ($param && $templet) {
            $body = $this->load->view('mail_template/' . $templet, $param, TRUE);
            if (isset($param['first_name']) && isset($param['last_name']) && !empty($param['first_name']) && !empty($param['last_name'])) {
                str_replace('{user_full_name}', $param['first_name'] . ' ' . $param['last_name'], $body);
            }
            //print_r($body); die;
            $this->email->message($body);
        } else {
            $this->email->message($message);
        }
        return $this->email->send() ? true : false;
    }

    function mcrypt_data($input) {
        /* Return mcrypted data */
        $key1 = "ShareSpark";
        $key2 = "Org";
        $key = $key1 . $key2;
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $input, MCRYPT_MODE_CBC, md5(md5($key))));
        //var_dump($encrypted);
        return $encrypted;
    }

    function demcrypt_data($input) {
        /* Return De-mcrypted data */
        $key1 = "ShareSpark";
        $key2 = "Org";
        $key = $key1 . $key2;
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($input), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
        return $decrypted;
    }

    function bcrypt_data($input) {
        $salt = substr(str_replace('+', '.', base64_encode(sha1(microtime(true), true))), 0, 22);
        $hash = crypt($input, '$2a$12$' . $salt);
        return $hash;
    }

    public function simplify_array($array, $key) {
        $returnArray = array();
        foreach ($array as $val) {
            $returnArray[] = $val[$key];
        }
        return $returnArray;
    }

    //Validate date
    function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    // for layout
    function load_views($customView, $data = array()) {
        $this->load->view('templates/header', $data);
        $this->load->view('templates/left_menu', $data);
        $this->load->view($customView, $data);
        $this->load->view('templates/footer', $data);
    }

    /**
     * Handle Pagination
     *
     * @access	public
     */
    public function handlePagination($totalRows) {

        //Load Pagination Library
        $this->load->config('pagination');
        $this->load->library('pagination');

        //First validate if there are any rows
        if ($totalRows > 0) {

            //Basic Pagination Config
            $finalSegment = $this->uri->segment(2);
            $config['per_page'] = $this->config->item('per_page_' . $finalSegment);
            $showMore = $this->input->get('show_more');
            $pageNumber = (!empty($showMore) and is_numeric($showMore)) ? $showMore - 1 : 0;
            $start = $config['per_page'] * $pageNumber;
            $config['total_rows'] = $totalRows;

            //Handle get params
            $additionalParams = '';
            $get = count($_GET) > 0 ? $_GET : array();
            $pageNumberKey = $this->config->item('query_string_segment');
            if (array_key_exists($pageNumberKey, $get)) {
                unset($get[$pageNumberKey]);
            }
            if (count($get) > 0) {
                $additionalParams = http_build_query($get);
            }
            $config['base_url'] = base_url() . 'index.php/view/' . $finalSegment . '?' . $additionalParams;
            $config['full_tag_open'] = '<div class="row"><div class="col-sm-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing ' . ($start + 1) . ' to ' . ($start + $config['per_page']) . ' of ' . $totalRows . ' entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example2_paginate"><ul class="pagination">';
            $this->pagination->initialize($config);

            return array(
                'totalRecords' => $config['total_rows'],
                'startCount' => $start
            );
        } else {
            return array(
                'totalRecords' => 0,
                'startCount' => 0
            );
        }
    }

    public function randomstring($length) {
        return $a = mt_rand(1000, 9999);
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //to validate email
    public function validate_email($e) {
        return (bool) preg_match("`^[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", trim($e));
    }

    public function encrypt($text, $salt, $isBaseEncode = true) {
        if ($isBaseEncode) {
            return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
        } else {
            return trim(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
        }
    }

    public function add($data) {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }

        $this->db->insert('jx_admin', $data);
        return $this->db->insert_id();
    }

    public function addin($data, $id) {

        $this->db->where('a_id', $id);
        $this->db->update('jx_admin', $data);
    }

    // activate account
//
//    function verifyEmail($key) {
//
//  
////    function is_email_available($email) {
////        $this->db->where('a_email', $email);
////        $query = $this->db->get("jx_admin");
////        if ($query->num_rows() > 0) {
////            return true;
////        } else {
////            return FALSE;
////        }
////    }      //  verifying the hashcode from email
//
//        $data = array(
//            'a_status' => 1
//        );
//        $this->db->where('emailVerificationCode', $key);
//        $this->db->update('jx_admin', $data);
//        return true;
////                $do = $this->db->last_query();
////print_r($do);die; //update status as 1 to make active user
//    }
////
//    public function emailcheck($phone) {
//        $this->db->where('a_phone', $phone);
//        $query = $this->db->get("jx_admin");
//        if ($query->num_rows() > 0) {
//            return true;
//        } else {
//            return FALSE;
//        }
//    }
//    public function ForgotPassword($email) {
//
//        $this->db->select('*');
//        $this->db->from('jx_admin');
//        $this->db->where('a_email', $email);
//        $query = $this->db->get();
//        return $query->row_array();
//    }

    function fetch_profile($id) {

        $this->db->select('a_name,a_email');
        $this->db->from('jx_admin');
        $this->db->where('a_id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_password($id, $data3, $data4) {
        $condition = "a_id =" . "'" . $id . "' AND " . "a_password =" . "'" . md5($data3['o_password']) . "'";
        // echo $condition;die;
        $this->db->where($condition);
        $this->db->update('jx_admin', $data4);

        if (md5($data3['o_password']) == $data4['a_password']) {

            $i = $this->db->affected_rows();
            if ($i == 0) {
                return true;
            }
        } else {

            return $this->db->affected_rows();
        }
    }

//    function edit($table, $data) {
//        // print_r($data);
//        // die;
//        $this->db->where('a_id', $data['a_id']);
//        $this->db->update($table, $data);
//
//
//        return true;
//    }

    public function fetch_user($table, $limit, $offset, $sort_by, $sort_order, $search, $userType) {
        $sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
        $sort_columns = array('user_name');
        $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'user_name';
        $search = ($search == '') ? '' : $search;

        if ($userType != "") {
            $this->db->where('status', $userType);
        }
        $where = "status !='3'";
        $this->db->where($where);
        $query = $this->db
                ->select('*')
                ->from('jx_user')
                ->where($where)
                ->like('user_name', $search, 'after')
                ->limit($limit, $offset)
                ->order_by($sort_by, $sort_order)
                ->get();
        if ($query) {    //echo $this->db->last_query();die;
            return ($query->result_array());
            //print_r($query->result_array());die;
        } else {
            return false;
        }
    }

    public function num_rows($search) {
        $this->db->select('*');
        $this->db->from('jx_user');

        if ($search != "") {

            $this->db->like('user_name', $search, 'after');
        }
        if ($query = $this->db->get())
            return $query->num_rows();
    }

    public function update_status($userid, $status) {
        $data['status'] = $status;
        //return $data; die;
        $this->db->where('user_id', $userid);
        $this->db->update('jx_user', $data);
        echo $this->db->last_query();die;
    }

}

?>