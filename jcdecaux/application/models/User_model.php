<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Expert_model
 *
 * @author 
 */
class User_model extends CI_Model {

    public $finalrole = array();

    public function __construct() {
        $this->load->database();
        $this->load->library('session');
    }
   /**
    * @method master_user_list
    * @param $select_key- array,$filter - array
    * @return  array
    * @description get all user using filter
    */
    public function master_user_list($select_key, $filter) {
        if (isset($select_key) && !empty($select_key)) {
            $this->db->select($select_key);
        } else {
            $this->db->select('*');
        }
        $this->db->from('pa_user');

        if (isset($filter['status']) && $filter['status'] != '') {
            $this->db->where('u_status', $filter['status']);
        }

        if (isset($filter['user_id']) && $filter['user_id'] != '') {
            $this->db->where('u_id', $filter['user_id']);
        }

        if (isset($filter['child_count']) && $filter['child_count'] != '') {
            $this->db->where('u_children_count', $filter['child_count']);
        }

        if (isset($filter['subscription']) && $filter['subscription'] != '') {
            $this->db->where('u_subscritption', $filter['subscription']);
        }

        if (isset($filter['gender']) && $filter['gender'] != '') {
            $this->db->where('u_gender', $filter['gender']);
        }

        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $from = str_replace('/', '-', $filter['from_date']);
            $from = date('Y-m-d', strtotime($from));
            $this->db->where('u_insert_date >= ', $from);
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $to = str_replace('/', '-', $filter['to_date']);
            $to = date('Y-m-d', strtotime($to));
            $this->db->where('u_insert_date <=', $to);
        }

        if (isset($filter['search']) && $filter['search'] != '') {
            $this->db->where("((u_email LIKE '".$filter['search']."%') OR (u_first_name LIKE '".$filter['search']."%') OR (u_first_name LIKE ' ".$filter['search']."%') OR (u_first_name LIKE '% ".$filter['search']."%'))");
        }

        if (isset($filter['order_type']) && isset($filter['order_by']) && $filter['order_type'] != '' && $filter['order_by'] != '') {
            if ($filter['order_by'] == 'name') {
                $order_by = 'u_first_name';
            } elseif ($filter['order_by'] == 'child_count') {
                $order_by = 'u_children_count';
            } elseif ($filter['order_by'] == 'reg_date') {
                $order_by = 'u_insert_date';
            }
            $this->db->order_by($order_by, $filter['order_type']);
        } else {
            $order_by = 'u_insert_date';
            $this->db->order_by($order_by, 'ASC');
        }

        if (isset($filter['limit']) && isset($filter['offset'])) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();die();
        if (isset($filter['result']) && $filter['result'] == 1) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
   /**
    * @method master_user_child_list
    * @param $select_key- array,$filter - array
    * @return  array
    * @description get all child using filter
    */
    public function master_user_child_list($select_key, $filter) {
        if (isset($select_key) && !empty($select_key)) {
            $this->db->select($select_key);
        } else {
            $this->db->select('*');
        }
        $this->db->from('pa_child');
        $this->db->join('pa_user user', 'user.u_id=c_father_id', 'left');
        $this->db->join('pa_school school', 'school.s_id=c_school_id', 'left');

        if (isset($filter['status']) && $filter['status'] != '') {
            $this->db->where('c_status', $filter['status']);
        }

        if (isset($filter['user_id']) && $filter['user_id'] != '') {
            $this->db->where('u_id', $filter['user_id']);
        }

        if (isset($filter['child_id']) && $filter['child_id'] != '') {
            $this->db->where('c_id', $filter['child_id']);
        }

        if (isset($filter['school_id']) && $filter['school_id'] != '') {
            $this->db->where('s_id', $filter['school_id']);
        }

        if (isset($filter['school_isverified']) && $filter['school_isverified'] != '') {
            if ($filter['school_isverified'] == 1) {
                $this->db->where('c_school_isverified', $filter['school_isverified']);
            } else {
                $this->db->where('c_school_isverified', 0);
            }
        }

        if (isset($filter['gender']) && $filter['gender'] != '') {
            $this->db->where('c_gender', $filter['gender']);
        }

        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $from = str_replace('/', '-', $filter['from_date']);
            $from = date('Y-m-d', strtotime($from));
            $this->db->where('c_insert_date >= ', $from);
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $to = str_replace('/', '-', $filter['to_date']);
            $to = date('Y-m-d', strtotime($to));
            $this->db->where('c_insert_date <=', $to);
        }

        if (isset($filter['search']) && $filter['search'] != '') {
            $this->db->like("SUBSTRING_INDEX(c_name,' ',1) ", $filter['search'], 'after');
            $this->db->or_like("SUBSTRING_INDEX(c_name,' ',-1) ", $filter['search'], 'after');
        }

        if (isset($filter['order_type']) && isset($filter['order_by']) && $filter['order_type'] != '' && $filter['order_by'] != '') {
            if ($filter['order_by'] == 'name') {
                $order_by = 'c_name';
            } elseif ($filter['order_by'] == 'reg_date') {
                $order_by = 'c_insert_date';
            }
            $this->db->order_by($order_by, $filter['order_type']);
        } else {
            $order_by = 'c_insert_date';
            $this->db->order_by($order_by, 'ASC');
        }

        if (isset($filter['limit']) && isset($filter['offset'])) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();die();
        return $query->result_array();
    }
   /**
    * @method user
    * @param $select_key- array,$filter - array
    * @return  array
    * @description get detail of user
    */
    public function user($select_key, $filter) {
        if (isset($select_key) && !empty($select_key)) {
            $this->db->select($select_key);
        } else {
            $this->db->select('*');
        }
        $this->db->from('pa_user');
        $this->db->join('country', 'country.country_code=pa_user.u_country', 'left');
        $this->db->join('states', 'states.state_code=pa_user.u_state AND states.country_code=pa_user.u_country', 'left');
        $this->db->join('weblocations city', 'city.id=pa_user.u_city', 'left');

        if (isset($filter['status']) && $filter['status'] != '') {
            $this->db->where('u_status', $filter['status']);
        }

        if (isset($filter['user_id']) && $filter['user_id'] != '') {
            $this->db->where('u_id', $filter['user_id']);
        }

        if (isset($filter['child_count']) && $filter['child_count'] != '') {
            $this->db->where('u_children_count', $filter['child_count']);
        }

        if (isset($filter['subscription']) && $filter['subscription'] != '') {
            $this->db->where('u_subscritption', $filter['subscription']);
        }

        if (isset($filter['gender']) && $filter['gender'] != '') {
            $this->db->where('u_gender', $filter['gender']);
        }

        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $from = str_replace('/', '-', $filter['from_date']);
            $from = date('Y-m-d', strtotime($from));
            $this->db->where('u_insert_date >= ', $from);
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $to = str_replace('/', '-', $filter['to_date']);
            $to = date('Y-m-d', strtotime($to));
            $this->db->where('u_insert_date <=', $to);
        }

        if (isset($filter['search']) && $filter['search'] != '') {
            $this->db->like("SUBSTRING_INDEX(u_first_name,' ',1) ", $filter['search'], 'after');
            $this->db->or_like("SUBSTRING_INDEX(u_last_name,' ',-1) ", $filter['search'], 'after');
        }

        if (isset($filter['order_type']) && isset($filter['order_by']) && $filter['order_type'] != '' && $filter['order_by'] != '') {
            if ($filter['order_by'] == 'name') {
                $order_by = 'u_first_name';
            } elseif ($filter['order_by'] == 'child_count') {
                $order_by = 'u_children_count';
            } elseif ($filter['order_by'] == 'reg_date') {
                $order_by = 'u_insert_date';
            }
            $this->db->order_by($order_by, $filter['order_type']);
        } else {
            $order_by = 'u_insert_date';
            $this->db->order_by($order_by, 'ASC');
        }

        if (isset($filter['limit']) && isset($filter['offset'])) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();die();
        if (isset($filter['result']) && $filter['result'] == 1) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    /*
     * dashboard data of user
     * not in use
     */

    public function dashboardOld($select_key, $filter) {
        if (isset($select_key) && !empty($select_key)) {
            $this->db->select($select_key);
        } else {
            $this->db->select("DATE_FORMAT(u_insert_date, '%Y-%m') AS month,COUNT(u_id) AS user");
        }
        $this->db->from('pa_user');

        if (isset($filter['limit']) && isset($filter['offset'])) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }
        $this->db->where("PERIOD_DIFF(DATE_FORMAT(NOW(), '%Y%m'), DATE_FORMAT(u_insert_date, '%Y%m'))<12");

        $this->db->group_by('YEAR(u_insert_date),YEAR(u_insert_date)');

        $query = $this->db->get();
        echo $this->db->last_query();
        die();

        if (isset($filter['result']) && $filter['result'] == 1) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
   /**
    * @method dashboard
    * @param $select_key- array,$filter - array
    * @return  array
    * @description get user data for dashboard
    */
    public function dashboard($select_key, $filter) {
        $res = array();
        $type = $filter['type'];
        if ($type == 'month') {
            $date_type = 'M-Y';
            $where = '%Y%m';
            $stmt = " Select DATE_FORMAT(u_insert_date,'%b-%Y')AS month,COUNT(u_id) AS user "; 
        } else if ($type == 'year') {
            $date_type = 'Y';
            $where = '%Y';
            $stmt = " Select DATE_FORMAT(u_insert_date,'%Y')AS year,COUNT(u_id) AS user";
        } else if ($type == 'week') {
            $date_type = 'W-Y';
            $where = '%Y%m';
            $stmt = " Select count(u_id) as user,MONTH(u_insert_date) AS month,CONCAT(WEEK(u_insert_date),'-',YEAR(u_insert_date))as week ";
        } else {
            $date_type = 'd-m-Y';
            $stmt = " Select count(u_id) as user,DATE_FORMAT(u_insert_date,'%d-%m-%Y') AS day ";
        }
        $stmt .= " from pa_user ";
        if ($type == 'month' || $type == 'year') {
            $stmt .= " where (PERIOD_DIFF(DATE_FORMAT(NOW(), '$where' ), DATE_FORMAT(u_insert_date, '$where' ))<12) ";
        } elseif ($type == 'week') {
            $stmt .= " where (u_insert_date BETWEEN DATE_SUB(NOW(), INTERVAL 84 DAY) AND NOW()) ";
        } else {
            $stmt .= " where (u_insert_date BETWEEN DATE_SUB(NOW(), INTERVAL 12 DAY) AND NOW()) ";
        }
        if ($type == 'month') {
            $stmt .= " group by  YEAR(u_insert_date), MONTH(u_insert_date) ";
        } else if ($type == 'year') {
            $stmt .= " group by  YEAR(u_insert_date) ";
        } else if ($type == 'week') {
            $stmt .= " group by  WEEK(u_insert_date) ";
        } else {
            $stmt .= " group by DATE_FORMAT(u_insert_date, '%d') ";
        }
        $query = $this->db->query($stmt);

        $resultSet = $query->result_array();
        //echo "<pre>";print_r($resultSet);die;
        if ($resultSet) {
            foreach ($resultSet AS $k => $v) {
                $res[$v[$type]] = $v['user'];
            }

            for ($i = 0; $i < 12; $i++) {
                if ($type == 'day' || $type == 'week')
                    $month = date($date_type, strtotime(" -$i $type"));
                else
                    $month = date($date_type, strtotime(date('Y-m-01') . " -$i $type"));
                $Record[$month] = $i;
            }
            //echo "<pre>";print_r($res);
            //echo "<pre>";print_r($Record);die;
            foreach ($Record as $key => $value) {
                if (array_key_exists($key, $res)) {
                    $monthRecord[$key] = $res[$key];
                } else {
                    $monthRecord[$key] = 0;
                }
            }


            return $monthRecord;
        } else {
            return array();
        }
    }
    

   /**
    * @method master_user_filter_list
    * @param $select_key- array,$filter - array
    * @return  array
    * @description get user data for push notification with filter
    */
    public function master_user_filter_list($select_key, $filter) {
        if (isset($select_key) && !empty($select_key)) {
            $this->db->select($select_key);
        } else {
            $this->db->select('*');
        }
        $this->db->from('pa_user');
        $this->db->join('pa_child child', 'pa_user.u_id=c_father_id', 'left');
        $this->db->join('pa_school school', 'school.s_id=c_school_id', 'left');

        if (isset($filter['status']) && $filter['status'] != '') {
            $this->db->where('u_status', $filter['status']);
        }

        if (isset($filter['user_id']) && $filter['user_id'] != '') {
            $this->db->where('u_id', $filter['user_id']);
        }

        if (isset($filter['child_count']) && $filter['child_count'] != '') {
            $this->db->where('u_children_count', $filter['child_count']);
        }

        if (isset($filter['subscription']) && $filter['subscription'] != '') {
            $this->db->where('u_subscritption', $filter['subscription']);
        }
        
        if (isset($filter['school_id']) && $filter['school_id'] != '') {
            $this->db->where('s_id', $filter['school_id']);
        }

        if (isset($filter['gender']) && $filter['gender'] != '') {
            $this->db->where('u_gender', $filter['gender']);
        }

        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $from = str_replace('/', '-', $filter['from_date']);
            $from = date('Y-m-d', strtotime($from));
            $this->db->where('u_insert_date >= ', $from);
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $to = str_replace('/', '-', $filter['to_date']);
            $to = date('Y-m-d', strtotime($to));
            $this->db->where('u_insert_date <=', $to);
        }

        if (isset($filter['search']) && $filter['search'] != '') {
            $this->db->like("SUBSTRING_INDEX(u_first_name,' ',1) ", $filter['search'], 'after');
            $this->db->or_like("SUBSTRING_INDEX(u_last_name,' ',-1) ", $filter['search'], 'after');
        }

        if (isset($filter['order_type']) && isset($filter['order_by']) && $filter['order_type'] != '' && $filter['order_by'] != '') {
            if ($filter['order_by'] == 'name') {
                $order_by = 'u_first_name';
            } elseif ($filter['order_by'] == 'child_count') {
                $order_by = 'u_children_count';
            } elseif ($filter['order_by'] == 'reg_date') {
                $order_by = 'u_insert_date';
            }
            $this->db->order_by($order_by, $filter['order_type']);
        } else {
            $order_by = 'u_insert_date';
            $this->db->order_by($order_by, 'ASC');
        }

        if (isset($filter['limit']) && isset($filter['offset'])) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();die();
        if (isset($filter['result']) && $filter['result'] == 1) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
    
   /**
    * @method get_alluser_push
    * @param $select_key- array,$filter - array
    * @return  array
    * @description get user data for push notification
    */
    public function get_alluser_push($select_key, $filter) {
        if (isset($select_key) && !empty($select_key)) {
            $this->db->select($select_key);
        } else {
            $this->db->select('*');
        }
        $this->db->from('pa_user');
        $this->db->join('pa_child child', 'child.c_father_id=pa_user.u_id', 'left');
        $this->db->join('pa_user_device user_device', 'user_device.ud_user_id=u_id AND ud_status=1', 'left');
           
        
        if(isset($filter['school_id'])&&$filter['school_id']!=''){
            $this->db->where_in('c_school_id',  explode(',', $filter['school_id']));
        }

        if (isset($filter['limit']) && isset($filter['offset'])) {
            $this->db->limit($filter['limit'], $filter['offset']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();die();
        if (isset($filter['result']) && $filter['result'] == 1) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
}
