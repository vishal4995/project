<?php
class Login extends Session_check
{

    /*

    Constructor for Library Functions,
    Such as Form validation, Session and Email sending.


    */

    function __construct()
    {
        parent::__construct();
   $this->load->model('Common_model');
        $this->load->helper('form', 'url');
        $this->load->library('session');
         $this->load->library('Session_check');
 $this->load->library('form_validation');
        $this->load->library('email');
 $this->load->helper('file');

        $this->load->database();



    }

/**
  *  //index Funcion for Viewing Index with Signup form.
 *@Param Default index
 *returns view.

*/

    public function index()
    {
       if($this->is_logged_in())
      { 
           redirect('admin/Dashboard');
      }
       else
      {
          
      

        $post=$this->input->post();
     
		if(!empty($post)){
                    //  print_r($post);die;
		$this->form_validation->set_rules('a_email','Email','trim|required');
		$this->form_validation->set_rules('a_password','Password','trim|required');
		if($this->form_validation->run() == TRUE)
		{
                      $data = array( 'a_email' => $this->input->post('a_email'), 'a_password' => $this->input->post('a_password')
  );
$res = $this->Common_model->fetch_data($data);

if($res == TRUE)
{
//print_r($res['a_id']);die;
  $sult['id']=$res['a_id'];
  //print_r($sult);die;
    $this->session->set_userdata($sult);
          //  print_r($rr);die;
        return redirect('admin/Dashboard');
}
 else {
  echo '<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';
                        echo '<script>
    setTimeout(function() {
        swal({
            title: "Incorrect Email or Password",
            text: "Email or Password did not match",
            type: "error"
        }, function() {
            window.location = "";
        });
    }, 1000);
</script>';

 
}
                }

      } 
 else {
      $this->load->view('Sign-in');  
      } }
      
     
 }


	 public function logout()
	{
     if($this->is_logged_in())
      { 
		$this->session->unset_userdata('a_id');
		$this->session->sess_destroy();
			redirect('admin/Login','refresh');
		}
                else
                {
                    redirect('admin/Login');
                }
        }

 }
?>