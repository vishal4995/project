<?php
class Signup extends MX_Controller
{

    /*

    Constructor for Library Functions,
    Such as Form validation, Session and Email sending.

    */



    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('form', 'url');

        $this->load->library('form_validation');
        $this->load->library('session');

        $this->load->library('email');


        $this->load->database();


    }


/**
  *  //index Funcion for Viewing Index with Signup form.
 *@Param Default index
 *returns view.

*/



    public function index()
    {
       
     
          $post=$this->input->post();

		if(!empty($post)){
        
         $this->form_validation->set_rules('a_name', 'Name', 'required|min_length[4]|max_length[20]|regex_match[/^[a-zA-Z. -]+$/]');
            $this->form_validation->set_rules('a_email', 'Email', 'required|valid_email|is_unique[jx_admin.a_email]');
            
            $this->form_validation->set_rules('a_password', 'Password', 'required|min_length[8]|max_length[20]');
            $this->form_validation->set_rules('cnfmPassword', 'Password Confirmation', 'trim|required|matches[a_password]');
            $this->form_validation->set_rules('a_phone', 'Phone number', 'required|is_unique[jx_admin.a_phone]|min_length[10]|max_length[10]|regex_match[/^(\+\d{1,3}[- ]?)?\d{10}$/]');
            

               if ($this->form_validation->run() == FALSE) {
             
            $this->load->view('Signup_view');
        } else {


            //posting input values into databse

            $data = array(
                
                'a_name' => $this->input->post('a_name'),
                'a_email' => $this->input->post('a_email'),
                'a_phone' => $this->input->post('a_phone'),
                'a_password' => md5($this->input->post('a_password')),
                'a_status' => 0,
                'a_insert_date' => date('Y-m-d H:i:s'),
                'emailVerificationCode' =>md5(rand())
                
            );


            //Transfering data to Model

            if ($this->Common_model->add($data)) {

                //send confirm mail
                if ($this->sendEmail($this->input->post('a_email'),$data['emailVerificationCode'])) {

                  

                } else {


                   

                }
            }
                } }
        else {
               $this->load->view('Signup_view');
        }
    }



    /**
      *  //index Funcion for Viewing Index with Signup form.
     *@Param Signup Success
     *returns success view.

    */


    public function inde()
    {
        $this->load->view('Success');
    }




    /**
      *    //fUNCTION TO INSERT DATA into database with Email verification.
    *
    * @Param Post Form data and validation
     *returns view.


    * s//fUNCTION TO INSERT DATA into database with Email verification.
    */


    // Confirming the email link function.



    function confirmEmail($hashcode)
    {
       // echo "hi";die;
        if ($this->Common_model->verifyEmail($hashcode)) {
            
            $this->load->view('Success');
           // redirect('Login');
        } else {
           
        }
    }


    //Checking if the Email is unique in database



    function check_email_availability()
    {
      $re =  $this->input->post('phone');

        if (!filter_var($re, FILTER_VALIDATE_EMAIL)) {
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Invalid Email</span></label>';
        } else {
           
            if ($this->Common_model->is_email_available($re)) {
                echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Email Already registered</label>';
            } else {
                echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> Email Available</label>';
            }
        }
    }



    //Checkinf if the Phone number is unique in database



    function check_phone_availability()
    {
        $phone = $this->input->post('phone');
      // print_r($phone);die;
        if (!is_numeric($phone)) {
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span>Enter valid number</label>';
        } else {

            if ($me = $this->Common_model->emailcheck($phone)) {

                echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Phone number Already registered</label>';

            } else {
                echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> Phone number Available</label>';

            }
        }
    }





//Establishing Country drop down with Cities

    public function sendEmail($receiver,$key)
  		{
  		$from = "vs83603@gmail.com"; //senders email address
  		$subject = 'Verify email address'; //email subject


$dak = array('hash' => $key);
  		// config email settings
      
  		$config['protocol'] = 'smtp';
  		$config['smtp_host'] = 'ssl://smtp.gmail.com';
  		$config['smtp_port'] = '465';
  		$config['smtp_user'] = $from;
  		$config['smtp_pass'] = 'vishal@4995'; //sender's password
  		$config['mailtype'] = 'html';
  		$config['charset'] = 'iso-8859-1';
  		$config['wordwrap'] = 'TRUE';
  		$config['newline'] = "\r\n";
  		$this->load->library('email', $config);
  		$this->email->initialize($config);

  		// send email

  		$this->email->from($from);
  		$this->email->to($receiver);
  		$this->email->subject($subject);
        $bod = $this->load->view('verify.php',$dak,TRUE);
  		$this->email->message($bod);
  		if ($this->email->send())
  			{
echo '<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';
                        echo '<script>
    setTimeout(function() {
        swal({
            title: "Verification Email Sent",
            text: "Verify your Email to Login",
            type: "success"
        }, function() {
            window.location = "";
        });
    }, 1000);
</script>';
  			}
  		  else
  			{
  			echo '<script>setTimeout(function () { swal("Email not sent");}, 1000);</script>';
  			return false;
  			}
  		}


}


?>
