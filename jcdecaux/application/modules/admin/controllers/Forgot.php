<?php

class Forgot extends MX_Controller {

   /**
    * Manage __construct
    *
    * @return Response
   */
   public function __construct() {
      parent::__construct();
         $this->load->model('Common_model');
        $this->load->library('form_validation');
        $this->load->database();
   $this->load->library('session');
   }
   /**
    * Manage_Index
    *
    * @return Response
   */


 public function index()
 {

     $post=$this->input->post();
if(!empty($post)){
    
         $email = $this->input->post('email');
         $findemail = $this->Common_model->ForgotPassword($email);
         if($findemail['a_id']){
         $code = array('forgotPasswordToken' => md5(rand())); 
         $this->Common_model->addin($code,$findemail['a_id']);
          $this->sendpassword($findemail['a_email'],$code);
           }else{
echo '<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';
                        echo '<script>
    setTimeout(function() {
        swal({
            title: "Email Not Found",
            text: "Entered Email is not registered!",
            type: "error"
        }, function() {
            window.location = "Forgot";
        });
    }, 1000);
</script>';
      }
}
 else {
     
     $this->load->view('myform');
}
   }


   /**
    * Manage __cEmail
    *@param email ID and Hashode
    * @return Response
   */

   function confirmEmail($hashcode){
   
     $verified = $this->Common_model->verify($hashcode);
//print_r($verified);die;
       if($verified) {
          $this->load->view('reset_password',array('hashcode' => $hashcode));
       }else{
            $this->load->view('reset_password',array('error' => "Error Occured"));
       }
   }


   /**
    * Manage __Password
    *
    * @return Response
   */

   function updatePassword() {


if(!isset($_POST['hashcode'])) {
  die("Error Updating your password");
}
$this->load->library('form_validation');
$this->form_validation->set_rules('hashcode', 'Email Hash', 'required|trim');
$this->form_validation->set_rules('a_password', 'password', 'required|min_length[8]|max_length[20]');
$this->form_validation->set_rules('cnfmPassword', 'Password Confirmation', 'trim|required|matches[a_password]');

if($this->form_validation->run() == FALSE)
{

  $this->load->view('reset_password');
}
else {

  $result = $this->Common_model->update_password();

  if($result)
  {

  $this-> update_password_suc();
  }
  else {
    $this->load->view('Forgot/confirmEmail', array('error' => "Problem Updating your password"));
  }
}

   }

function update_password_suc() {

  $this->load->view('update_password_success');
}


 public function sendpassword($email,$hash)
{
   
  $ret =  $this->Common_model->fetch($email);




        $data = array(
             'hash'=> $hash['forgotPasswordToken'],
            'name' => $ret[0]['a_name']
                 );
               
        $from    = "vs83603@gmail.com"; //senders email address
        $subject = 'Reset Password'; //email subject

        $config['protocol']  = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_user'] = $from;
        $config['smtp_pass'] = 'vishal@4995'; //sender's password
        $config['mailtype']  = 'html';
        $config['charset']   = 'iso-8859-1';
        $config['wordwrap']  = 'TRUE';
        $config['newline']   = "\r\n";

        $this->load->library('email', $config);
        $this->email->initialize($config);
        //send email
        $this->email->from($from);
        $this->email->to($email);
        $this->email->subject($subject);
        $body = $this->load->view('mail_verify.php',$data,TRUE);
        $this->email->message($body);


        if ($this->email->send()) {
           

        } else {
           
        }
        redirect(base_url() . 'Forgot', 'refresh');
    } 
 } 
        ?>
