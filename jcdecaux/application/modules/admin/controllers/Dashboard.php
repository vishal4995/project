<?php

class Dashboard extends Session_check {
    /*

      Constructor for Library Functions,
      Such as Form validation, Session and Email sending.

     */

    function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('form', 'url');
        $this->load->library('Session_check');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->config->load('pagination_config', true);
    }

    /**
     *  //index Funcion for Viewing Index with Dashboard.
     * @Param Default index
     * returns view.

     */
    public function index($sort_by = 'user_name', $sort_order = 'asc') {
        if ($this->is_logged_in()) {

            $search = ($this->input->get('search') != "") ? trim($this->input->get('search')) : "";
            $data['search'] = $search;
            $userType = ($this->input->get('userType') != "") ? $this->input->get('userType') : "";
            $data['userType'] = $userType;

            $setting = $this->config->item('pagination_config');
            $per_page = $this->config->item('per_page', 'pagination_config');
            $setting["base_url"] = base_url("admin/Dashboard/index/$sort_by/$sort_order");
            $setting['total_rows'] = $this->Common_model->num_rows($search);
            $this->pagination->initialize($setting);
            $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;

            $data['data'] = $this->Common_model->fetch_user('jx_user', $per_page, $page, $sort_by, $sort_order, $search, $userType);
            $data['links'] = $this->pagination->create_links();
            $data['sort_by'] = $sort_by;
            $data['sort_order'] = $sort_order;

            if ($data) {

                $this->load->view('dash', $data);
            } else {
                //echo "sd";die;
                redirect('admin/Dashboard');
            }
        } else {
            redirect('admin/Login');
        }
    }

    function change_status() {
//        echo 'sdasdfaf';die;
//        echo $this->input->post('status');die;
        
        if ($this->is_logged_in()) {
            $status = $this->input->post('status');
            $userid = $this->input->post('id');
            $this->Common_model->update_status($userid, $status);
        } else {
            redirect('admin/Dashboard');
        }
    }

}
?>

