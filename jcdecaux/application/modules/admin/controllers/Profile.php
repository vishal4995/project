<?php
class Profile extends Session_check
{

    /*

    Constructor for Library Functions,
    Such as Form validation, Session and Email sending.

    */

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('form', 'url');
        $this->load->library('session');
 $this->load->library('form_validation');
        $this->load->library('email');
 $this->load->helper('file');
  $this->load->library('Session_check');
        $this->load->database();

    }

/**
  *  //index Funcion for Viewing Index with Signup form.
 *@Param Default index
 *returns view.

*/
    public function index()
    {
   if($this->is_logged_in()) {
    $id=$this->session->userdata('id');
   
   $result = $this->Common_model->fetch_profile($id);
   
        $this->load->view('Profile',$result);
        
   }
 else {
       redirect('admin/Login');
   }
    }
//     public function edit()
//     {
//           $id=$this->session->userdata('id');
//   $resul = $this->Common_model->fetch_profile($id);
//      $this->load->view('Edit',$resul);
//      if(!empty($this->input->post())){
//             
//		$data1=$this->input->post();
//                $data1['a_profile_image'] = $_FILES['a_profile_image']['name'];
//             
//                $this->form_validation->set_rules('a_name','Name','required|min_length[4]|max_length[20]|regex_match[/^[a-zA-Z. -]+$/]');
//                 $this->form_validation->set_rules('a_email', 'Email', 'required|valid_email');
//                  $this->form_validation->set_rules('a_profile_image', 'image', 'required');
//		 if ($this->form_validation->run() == TRUE) 
//                {
//                   
//		$file_tmp = $_FILES['a_profile_image']['tmp_name'];
//		 $file_name = time().".".$_FILES['a_profile_image']['name'];
//                  require_once(APPPATH.'libraries/S3.php');
//$s3 = new S3(access_key, secret_key);
//
//$s3->putBucket(Bucket, S3::ACL_PUBLIC_READ);
//
// if($s3->putObjectFile($file_tmp, Bucket , $file_name, S3::ACL_PUBLIC_READ) )
//{
//	 $s3file=$file_name;
//	 $data1['a_profile_image']=$s3file;
//
//     }
//
//     $data1['a_id']=$this->session->userdata('id');
//	$record_id=$this->Common_model->edit('jx_admin',$data1);
//        
// }
// else {
//
//
//redirect('admin/Profile/edit');
// }
//     } } 
     
     public function change_password()
     {
          if($this->is_logged_in()) {
              
         $id=$this->session->userdata('id');
         $this->load->view('Change_password');
        if(!empty($this->input->post()))
            {
         
               $data2=$this->input->post();
   
               $data3 = array('o_password' => $this->input->post('o_password'),
          'a_password' => $this->input->post('a_password'));
     
      $data4 = array('a_password' => md5($this->input->post('a_password')));
      
          $res = $this->Common_model->update_password($id,$data3,$data4);
         
         
         if($res)
         {
             $this->load->view('alert');

         }
 else {
  $this->load->view('false_alert');
 }
          } }

          else 
          {
               redirect('admin/Login');
          }
 }
     

	
}
?>