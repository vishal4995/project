<!DOCTYPE html>
<html>
<head>
    
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PROJECT</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/bootstrap.min.css">
  <!-- Font Awesome -->
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
<link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/blue.css">
  <!-- custom css -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/style.css">

  <!-- jQuery 2.2.3 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body class="hold-transition login-page" style="background:#3c8dbc;">


  <!-- Sign Up form for Admin Panel-->


<div class="login-box">
  <div class="login-logo" style="color:white">

  </div>

    <div style="display : none" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <i class="icon fa fa-ban"></i>
          </div>

    <div style="display : none" class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <i class="icon fa fa-check"></i>
          </div>

  <!-- Custom Tabs -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
    <center> <h2>SIGN UP</h2></center>
    </ul>

    <div class="tab-content">
      <div class="tab-pane active" id="">
        <!-- /.login-logo -->
        <div class="login-box-body">
      <?php    $at = array('name' => 'userSignup', 'id' => 'userSignup');
           echo form_open_multipart('Signup',$at); ?>

           
            
              <h5><b>Name</b></h5>
            <div class="form-group has-feedback" style="padding-bottom:10px;">

                <div>
                  <input type="text" class="form-control" name="a_name" id="userName" placeholder="Name" value="" autofocus="autofocus">
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  <span class="form_error error" id="err_userName"></span>
                  <span><?php echo form_error('a_name'); ?></span>
                </div>
            </div>
              <h5><b>Email</b></h5>
            <div class="form-group has-feedback" style="padding-bottom:10px;">

                <div>
                  <input type="text" class="form-control" name="a_email" id="userEmail" placeholder="Email" autofocus="autofocus">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <span id="email_result"></span>
                    <span class="form_error error" id="err_mobileNo"></span>
                <span><?php echo form_error('a_email'); ?></span>
                </div>
            </div>

                <div class="form-group has-feedback" style="padding-bottom:10px;">
                <h5><b>Mobile No.</b></h5>
                <div class="clearfix">
                    <select style=" width: 20%;float: left;padding-right: 0;margin-right: 10px;"class="form-control" name="countrycode" id="countrycode" >

                    </select>
                  <input style="width: 76%;float: left;" type="text" class="form-control" name="a_phone" id="mobileNo" placeholder="Mobile Number" value="<?php echo set_value('mobileNo'); ?>" autofocus="autofocus">
                  <span class="glyphicon glyphicon- form-control-feedback"></span>
                    <span id="phone_result"></span>
                  <span class="form_error error" id="err_mobileNo"></span>
                  <span><?php echo form_error('a_phone'); ?></span>
                </div>
            </div>

           

              <h5><b>Password</b></h5>
            <div class="form-group has-feedback">

                <div>
                  <input type="password" class="form-control" name="a_password" id="a_password" placeholder="Password" autofocus="autofocus">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  <span class="form_error error" id="err_userPassword"></span>
                    <span><?php echo form_error('a_password'); ?></span>
                </div>
            </div><br>
              <h5><b>Confirm Password</b></h5>
            <div class="form-group has-feedback">

                <div>
                  <input type="password" class="form-control" name="cnfmPassword" id="cnfmPassword" placeholder="Re-enter Password" autofocus="autofocus">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  <span class="form_error error" id="err_cnfmPassword"></span>
                </div>
            </div></br>
           
            <div class="row">
              <div class="col-xs-8">
                <div class="checkbox icheck">
                  <label>
                    <!--<input type="checkbox"> Remember Me-->
                  </label>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-xs-4">
                <button type="submit" value="submit" class="btn btn-primary btn-block btn-flat">Sign up</button>
              </div>
              <!-- /.col -->
              <div class="col-xs-12" style="margin-top: 20px; text-align: center">
                  Already have an account? <a href="<?php echo base_url('Login'); ?>" >Sign in</a>
              </div>
            </div>
         <br>
        </div>
        <!-- /.login-box-body -->
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
  <!-- nav-tabs-custom -->
</div>
<!-- /.login-box -->

<!-- Bootstrap 3.3.6 -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="http://localhost/jcdecaux/public/js/icheck.min.js"></script>
<script src="http://localhost/jcdecaux/public/js/jquery.validate.js"></script>
<script src="http://localhost/jcdecaux/public/js/additional-methods.js"></script>
<script src="http://localhost/jcdecaux/public/js/ajax.js"></script>
<script src="http://localhost/jcdecaux/public/js/jquery.validation.js"></script>
  <script src="http://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
     $('#userEmail').change(function(){
          var phone = $('#userEmail').val();
          if(phone != '')
          {

               $.ajax({
                    url:"http://localhost/jcdecaux/admin/Signup/check_email_availability",
                    method:"POST",
                    data:{phone:phone},
                    success:function(data){
                         $('#email_result').html(data);
                    }
               });
          }
     });
});

$(document).ready(function(){
     $('#mobileNo').change(function(){
          var phone = $('#mobileNo').val();
          if(phone != '')
          {
               $.ajax({
                    url:"http://localhost/jcdecaux/admin/Signup/check_phone_availability",
                    method:"POST",
                    data:{phone:phone},
                    success:function(data){
                         $('#phone_result').html(data);
                    }
               });
          }
     });
});

 </script>


</body>

</html>
