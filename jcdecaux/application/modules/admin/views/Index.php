
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Mannat Themes">
        <meta name="keyword" content="">

        <title>JCDecaux</title>

        <!-- Theme icon -->
        <link rel="shortcut icon" href="http://localhost/jcdecaux/public/img/favicon.ico">

        <link href="http://localhost/jcdecaux/public/css/morris.css" rel="stylesheet">
        <!-- Theme Css -->
        <link href="http://localhost/jcdecaux/public/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://localhost/jcdecaux/public/css/slidebars.min.css" rel="stylesheet">
        <link href="http://localhost/jcdecaux/public/css/icons.css" rel="stylesheet">
        <link href="http://localhost/jcdecaux/public/css/menu.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/jcdecaux/public/css/style.css" rel="stylesheet">
    </head>

    <body class="sticky-header">
        <section>
            <!-- sidebar left start-->
            <div class="sidebar-left" style="background:#323232;">
                <div class="sidebar-left-info">

                    <div class="user-box">
                      <center>  <div class="d-flex justify-content-center">
                            <img src="http://localhost/jcdecaux/public/img/noimage.png" alt="" class="img-fluid rounded-circle"> 
                        </div></center>
                        <div class="text-center text-white mt-2">
                            <h6>Travis Watson</h6>
                            <p class="text-muted m-0">Admin</p>
                        </div>
                    </div>   
                                        
                    <!--sidebar nav start-->
                    <ul class="side-navigation">
                        <li>
                            <h3 class="navigation-title">Navigation</h3>
                        </li>
                        <li class="active">
                            <a href="index.html"><i class="mdi mdi-gauge"></i> <span>Dashboard</span></a>
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-buffer"></i> <span>UI Elements</span></a>
                            <ul class="child-list">
                                <li><a href="ui-typography.html"> Typography</a></li>
                                <li><a href="ui-buttons.html"> Buttons</a></li>
                                <li><a href="ui-cards.html"> Cards</a></li>
                                <li><a href="ui-tabs.html"> Tab & Accordions</a></li>
                                <li><a href="ui-modals.html"> Modals</a></li>
                                <li><a href="ui-bootstrap.html"> BS Elements</a></li>
                                <li><a href="ui-progressbars.html"> Progress Bars</a></li>
                                <li><a href="ui-notification.html">Notification</a></li>
                                <li><a href="ui-carousel.html"> Carousel</a></li>
                            </ul>
                        </li>
                        <li>
                            <h3 class="navigation-title">Components</h3>
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-google-circles-extended"></i> <span>Components <span
                                class="badge badge-primary noti-arrow pull-right">6</span> </span></a>
                            <ul class="child-list">
                                <li><a href="components-grid.html"> Grid</a></li>
                                <li><a href="components-calendar.html"> Calendar</a></li>
                                <li><a href="components-sweet-alerts.html"> Sweet Alerts </a></li>
                                <li><a href="components-portlets.html"> Portlets </a></li>
                                <li><a href="components-widgets.html"> Widgets </a></li>
                                <li><a href="components-nestable.html"> Nestable </a></li>
                                <li><a href="components-range-slider.html"> Range Slider </a></li>
                            </ul>
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-diamond"></i> <span>Icons</span></a>
                            <ul class="child-list">
                                <li><a href="icons-material.html"> Material Design</a></li>
                                <li><a href="icons-font-awesome.html"> Font Awesome</a></li>
                                <li><a href="icons-themify.html"> Themify</a></li>
                            </ul>
                        </li>
                        <li class="menu-list"><a href="javascript:;"><i class="mdi mdi-table"></i> <span>Tables</span></a>
                            <ul class="child-list">
                                <li><a href="tables-basic.html"> Basic Table</a></li>
                                <li><a href="tables-datatable.html"> Data Table</a></li>
                                <li><a href="tables-editable.html"> Editable </a></li>
                                <li><a href="tables-responsive.html"> Responsive Table </a></li>
                            </ul>
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-google-earth"></i> <span>Forms</span></a>
                            <ul class="child-list">
                                <li><a href="form-elements.html">General Elements</a></li>
                                <li><a href="form-validation.html">Form Validation</a></li>
                                <li><a href="form-advanced.html">Advanced Form</a></li>
                                <li><a href="form-wizard.html">Form Wizard</a></li>
                                <li><a href="form-editor.html">WYSIWYG Editor</a></li>
                                <li><a href="form-uploads.html">Multiple File Upload</a></li>
                                <li><a href="form-imagecrop.html">Image Crop</a></li>
                                <li><a href="form-xeditable.html">X-Editable</a></li>
                                <li><a href="form-summernote.html">Summernote</a></li>
                            </ul>
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-chart-arc"></i> <span>Charts </span></a>
                            <ul class="child-list">
                                <li><a href="chart-morris.html">Morris Chart</a></li>
                                <li><a href="chart-chartjs.html">Chartjs</a></li>
                                <li><a href="chart-flot.html">Flot Chart</a></li>
                                <li><a href="chart-peity.html">Peity Chart</a></li>
                                <li><a href="chart-knob.html">Knob Chart</a></li>
                            </ul>
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-email" aria-hidden="true"></i><span>Mail </span></a>
                           
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-newspaper" aria-hidden="true"></i><span>Email Templates</span></a>
                          
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-map" aria-hidden="true"></i><span>Maps</span></a>
                           
                        </li>
                        <li class="menu-list"><a href=""><i class="mdi mdi-book-multiple" aria-hidden="true"></i><span>Pages</span></a>
                         
                        </li>
                    </ul><!--sidebar nav end-->
                </div>
            </div><!-- sidebar left end-->

            <!-- body content start-->
            <div class="body-content">
                <!-- header section start-->
                <div class="header-section">
                    <!--logo and logo icon start-->
                    <div class="logo">
                        <a href="index.html">
                         
                            <!--<i class="fa fa-maxcdn"></i>-->
                            <span class="brand-name">JCDEcaux</span>
                        </a>
                    </div>

                    <!--toggle button start-->
                    <a class="toggle-btn"><i class="ti ti-menu"></i></a>
                    <!--toggle button end-->

                
                    <!--mega menu end-->

                    <div class="notification-wrap">
                        <!--right notification start-->
                        <div class="right-notification">
                            <ul class="notification-menu">
                                <li>
                                  
                                    
                                </li>

                                <li>
                                 
                                    <ul class="dropdown-menu mailbox dropdown-menu-right">
                                        <li>
                                          
                                        </li>
                                        
                                        <li> 
                                            <a class="text-center bg-light" href="javascript:void(0);"> <strong>See all notifications</strong> </a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="javascript:;" data-toggle="dropdown">
                                        <img src="http://localhost/jcdecaux/public/img/avatar-1.jpg" alt="">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-menu">
                                        <a class="dropdown-item" href="<?php echo base_url('Profile');?>"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a>
                                       
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                    </div>

                                </li>

                              
                            </ul>
                        </div><!--right notification end-->
                    </div>
                </div>
                <!-- header section end-->

                <div class="container-fluid">
                    <div class="page-head">
                        <h4 class="mt-2 mb-2">Dashboard</h4>
                    </div>
                                            
                    
                </div><!--end container-->
 </div>
                <!--footer section start-->
                <footer class="footer">
                    2018 &copy; Syntra.
                </footer>
                <!--footer section end-->


                <!-- Right Slidebar start -->
                <div class="sb-slidebar sb-right sb-style-overlay">
                    <div class="right-bar slimscroll">
                        <span class="r-close-btn sb-close"><i class="fa fa-times"></i></span>

                        <ul class="nav nav-tabs nav-justified-">
                            <li class="">
                                <a href="#chat" class="active" data-toggle="tab">Chat</a>
                            </li>
                            <li class="">
                                <a href="#activity" data-toggle="tab">Activity</a>
                            </li>
                            <li class="">
                                <a href="#settings" data-toggle="tab">Settings</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="chat">
                                <div class="online-chat">
                                    <div class="online-chat-container">
                                        <div class="chat-list">
                                            <form class="search-content" action="index.html" method="post">
                                                <input type="text" class="form-control" name="keyword" placeholder="Search...">
                                                <span class="search-button"><i class="ti ti-search"></i></span>
                                            </form>
                                        </div>
                                        <div class="side-title-alt">
                                            <h2>online</h2>                                           
                                        </div>

                                        <ul class="team-list chat-list-side">
                                            <li>
                                                <a href="#" class="ml-3">
                                                    <span class="thumb-small">
                                                        <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-1.jpg" alt="">
                                                        <i class="online dot"></i>
                                                    </span>
                                                    <div class="inline">
                                                        <span class="name">Alison Jones</span>
                                                        <small class="text-muted">Start exploring</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="ml-3">
                                                    <span class="thumb-small">
                                                        <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-3.jpg" alt="">
                                                        <i class="online dot"></i>
                                                    </span>
                                                    <div class="inline">
                                                        <span class="name">Jonathan Smith</span>
                                                        <small class="text-muted">Alien Inside</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="ml-3">
                                                    <span class="thumb-small">
                                                        <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-4.jpg" alt="">
                                                        <i class="away dot"></i>
                                                    </span>
                                                    <div class="inline">
                                                        <span class="name">Anjelina Doe</span>
                                                        <small class="text-muted">Screaming...</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="ml-3">
                                                    <span class="thumb-small">
                                                        <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-5.jpg" alt="">
                                                        <i class="busy dot"></i>
                                                    </span>
                                                    <div class="inline">
                                                        <span class="name">Franklin Adam</span>
                                                        <small class="text-muted">Don't lose the hope</small>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="ml-3">
                                                    <span class="thumb-small">
                                                        <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-6.jpg" alt="">
                                                         <i class="online dot"></i>
                                                    </span>
                                                    <div class="inline">
                                                        <span class="name">Jeff Crowford </span>
                                                        <small class="text-muted">Just flying</small>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="side-title-alt mb-3">
                                            <h2>Friends</h2>
                                        </div>
                                        <ul class="list-unstyled friends">
                                            <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-7.jpg" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-8.jpg" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-9.jpg" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-10.jpg" alt="">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-2.jpg" alt="">
                                                </a>
                                            </li>
                                             <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-1.jpg" alt="">
                                                </a>
                                            </li>
                                             <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-3.jpg" alt="">
                                                </a>
                                            </li>
                                             <li>
                                                <a href="#">
                                                    <img class="rounded-circle" src="http://localhost/jcdecaux/public/img/avatar-4.jpg" alt="">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane " id="activity">
                                
                                <div class="aside-widget">
                                    <div class="side-title-alt">
                                        <h2>Recent Activity</h2>
                                    </div>
                                    <ul class="team-list chat-list-side info">
                                        <li>
                                            <span class="thumb-small">
                                                <i class="fa fa-pencil"></i>
                                            </span>
                                            <div class="inline">
                                                <span class="name">Mary Brown open a new company</span>
                                                <span class="time">just now</span>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="thumb-small">
                                                <i class="fa fa-user-plus"></i>
                                            </span>
                                            <div class="inline">
                                                <span class="name">Mary Brown send a new message </span>
                                                <span class="time">50 min ago</span>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="thumb-small">
                                                <i class="fa fa-wrench"></i>
                                            </span>
                                            <div class="inline">
                                                <span class="name">Holly Cobb Uploaded 6 new photos.</span>
                                                <span class="time">3 Week Ago</span>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="thumb-small">
                                                <i class="fa fa-users"></i>
                                            </span>
                                            <div class="inline">
                                                <span class="name">Mary Brown open a new company.</span>
                                                <span class="time">1 Month Ago</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="aside-widget">

                                    <div class="side-title-alt">
                                        <h2>Events</h2>
                                    </div>
                                    <ul class="team-list chat-list-side info statistics border-less-list">
                                        <li>
                                            <div class="inline">
                                                <p class="mb-1">Jamie Miller</p>
                                                <span class="name">Contrary to popular belief, Lorem Ipsum is not simply random text.</span>
                                                <span class="time text-muted">2 Week Ago</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="inline">
                                                <p class="mb-1">Robert Jones</p>
                                                <span class="name">Lorem Ipsum is simply dummy text of the printing and typesetting .</span>
                                                <span class="time text-muted">1 Month Ago</span>
                                            </div>
                                        </li>                                        
                                    </ul>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane " id="settings">
                                <div class="side-title-alt">
                                    <h6 class="mb-0">Account Setting</h6>
                                </div>
                                <ul class="team-list chat-list-side info statistics border-less-list setting-list">
                                    <li>
                                        <div class="inline">
                                            <span class="name">Auto updates</span>
                                        </div>
                                        <span class="thumb-small">
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#079c9e" data-size="small"/>
                                        </span>
                                    </li>
                                    <li>
                                        <div class="inline">
                                            <span class="name">Show offline Contacts</span>
                                        </div>
                                        <span class="thumb-small">
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#079c9e" data-size="small"/>
                                        </span>
                                    </li>

                                    <li>
                                        <div class="inline">
                                            <span class="name">Location Permission</span>
                                        </div>
                                        <span class="thumb-small">
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#079c9e" data-size="small"/>
                                        </span>
                                    </li>
                                </ul>

                                <div class="side-title-alt">
                                    <h6 class="mb-0">General Setting</h6>
                                </div>
                                <ul class="team-list chat-list-side info statistics border-less-list setting-list">
                                    <li>
                                        <div class="inline">
                                            <span class="name">Show me Online</span>
                                        </div>
                                        <span class="thumb-small">
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#079c9e" data-size="small"/>
                                        </span>
                                    </li>
                                    <li>
                                        <div class="inline">
                                            <span class="name">Status visible to all</span>
                                        </div>
                                        <span class="thumb-small">
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#079c9e" data-size="small"/>
                                        </span>
                                    </li>

                                    <li>
                                        <div class="inline">
                                            <span class="name">Notifications</span>
                                        </div>
                                        <span class="thumb-small">
                                            <input type="checkbox" checked data-plugin="switchery" data-color="#079c9e" data-size="small"/>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end Right Slidebar-->
            </div>
            <!--end body content-->
        </section>

        <!-- jQuery -->
        <script src="http://localhost/jcdecaux/public/js/jquery-3.2.1.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/popper.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/bootstrap.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/jquery-migrate.js"></script>
        <script src="http://localhost/jcdecaux/public/js/modernizr.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/jquery.slimscroll.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/slidebars.min.js"></script>

        <!--plugins js-->
        <script src="http://localhost/jcdecaux/public/js/jquery.counterup.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/jquery.waypoints.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/jquery.sparkline.min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/jquery.sparkline.init.js"></script>

        <script src="http://localhost/jcdecaux/public/js/Chart.bundle.js"></script>
        <script src="http://localhost/jcdecaux/public/js/raphael-min.js"></script>
        <script src="http://localhost/jcdecaux/public/js/morris.js"></script>
        <script src="http://localhost/jcdecaux/public/js/dashboard-init.js"></script>


        <!--app js-->
        <script src="http://localhost/jcdecaux/public/js/jquery.app.js"></script>
        <script>
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                delay: 100,
                time: 1200
                });
            });
        </script>
    </body>
</html>
