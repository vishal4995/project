<?php if(isset($_SESSION['msgsent'])){
    echo '<script>setTimeout(function () { swal("Reset link sent to your Email");}, 500);</script>';
 }?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PROJECT</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/blue.css">
  <!-- custom css -->
  <link rel="stylesheet" href="http://localhost/jcdecaux/public/asset/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- jQuery 2.2.3 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body class="hold-transition login-page" style="background:#3c8dbc;">


  <!-- Sign Up form for Admin Panel-->


<div class="login-box">
  <div class="login-logo" style="color:white">

  </div>

    <div style="display : none" class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <i class="icon fa fa-ban"></i>
          </div>

    <div style="display : none" class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <i class="icon fa fa-check"></i>
          </div>

  <!-- Custom Tabs -->
    <form id="resetPassword" name="resetPassword" method="post" action="http://localhost/jcdecaux/admin/Forgot">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
    <center> <h2>Forgot Password</h2></center>
    </ul>
  &nbsp;&nbsp;  <label>Enter Email</label>&nbsp;
    <input type="email" name="email" id="email" style="width:250px" required>
<br><br>
<center>
        <!-- /.login-logo -->  <div class="col-xs-4">
        <button type="submit" value="submit" class="btn btn-primary btn-block btn-flat">Submit</button></div></center>
        <div class="login-box-body">

         <br>
        </div>
        <!-- /.login-box-body -->

      <!-- /.tab-pane -->

    <!-- /.tab-content -->
  </div></form>
  <!-- nav-tabs-custom -->
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="http://localhost/jcdecaux/public/js/icheck.min.js"></script>
<script src="http://localhost/jcdecaux/public/js/jquery.validate.js"></script>
<script src="http://localhost/jcdecaux/public/js/additional-methods.js"></script>
<script src="http://localhost/jcdecaux/public/js/jquery.validation.js"></script>
  <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
</body>
</html>
