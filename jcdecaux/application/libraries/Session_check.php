<?php
class Session_check extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function is_logged_in()
    {
        $user = $this->session->userdata('id');
        return isset($user);
    }
}
?>