

$.validator.addMethod("hello", function(value, element)
{
return this.optional(element) || value.length>=8 && /\d/.test(value) && /[a-z]/i.test(value);
}, "Please enter atleast 8 characters which contains atleast 1 letter, 1 number and 1 special character");


$("#register").validate({
    errorElement: 'span',
    rules: {

             o_password: {
            normalizer: function( value ) {
                                return $.trim( value );
                        },
            required:  true,
            minlength: 6,
            maxlength: 50,
            hello: true
        },
           a_password: {
            normalizer: function( value ) {
                                return $.trim( value );
                        },
            required:  true,
            minlength: 6,
            maxlength: 50,
            hello: true
        },
           cnfmpassword: {
            normalizer: function( value ) {
                                return $.trim( value );
                        },
            required:  true,
              equalTo:   "#a_password"
        }
      
    },
    messages: {
      
       o_password : {
             required:  "Current Password is required",
            minlength: "Current Password is too short",
            maxlength: "Current Password is too long"
        },
        a_password : {
             required:  "New Password is required",
            minlength: "New Password is too short",
            maxlength: "New Password is too long"
        },
       
    
        cnfmpassword : {
            required:  "Confirm Password is required",
           equalTo: "Password Mismatch"
           
        }
      
    }
});
