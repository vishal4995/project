$( document ).ready(function() {
    
    jQuery.validator.addMethod("validEmail", function (value, element)
{
   return this.optional(element) || /^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/i.test(value);
}, "Enter valid email address");

$.validator.addMethod("hello", function(value, element)
{
return this.optional(element) || value.length>=8 && /\d/.test(value) && /[a-z]/i.test(value);
}, "Please enter atleast 8 characters which contains atleast 1 letter, 1 number and 1 special character");

$("#login").validate({
    errorElement: 'span',
    rules: {

      
        a_email : {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true,
            maxlength: 100,
            email: true,
            validEmail: true
        },
       
        
        a_password : {
            normalizer: function( value ) {
                                return $.trim( value );
                        },
            required:  true,
            minlength: 6,
            maxlength: 50,
            hello: true
        }
    
    },
    messages: {
      
     
        a_email : {
            required: "Enter email address",
            maxlength: "Email Address is Too Long",
            email: "Enter valid email address",
            validEmail: "Enter valid email address"
        },
      
        a_password : {
            required:  "Password is required",
            minlength: "Password is too short",
            maxlength: "Password is too long",
            hello: "Please enter atleast 8 characters which contains atleast 1 letter, 1 number and 1 special character"
        }
  
    }
});
});