
<?php
class Dashboard extends CI_controller

	{
	/*
	Constructor for Library Functions,
	Such as Form validation, Session and Email sending.
	*/
	function __construct()
		{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->database();
		}

	/**
	 *  //index Funcion for Viewing Index with Signup form.
	 *@Param Default index
	 *returns view.
	 */
	public

	function index()
		{
		$id = $this->session->userdata('id');

		// echo $id;die;

		if (!empty($this->input->post()))
			{
			$post = $this->input->post();
			if (!empty($post))
				{

				//  print_r($post);die;

				$this->form_validation->set_rules('sname', 'Email', 'trim|required');
				$this->form_validation->set_rules('sdesc', 'Password', 'trim|required');
				if ($this->form_validation->run() == TRUE)
					{
					$data = array(
						'title' => $this->input->post('sname') ,
						'description' => $this->input->post('sdesc')
					);
					$res = $this->Common_model->insert_single('survey', $data);
					if ($res)
						{

						//      echo $res;die;

						$sult['s_id'] = $res;
						$this->session->set_userdata($sult);
						return redirect('index.php/Dashboard/survey_table');
						}
					}
				}
			}
		  else
			{
			$this->load->view('dash');
			}
		}

	public

	function survey_table()
		{
		$result = $this->Common_model->fetch();
		if ($result)
			{
			$this->load->view('table', $result);
			}
		}

	public

	function survey()
		{
		$id = $this->session->userdata('s_id');
		if (!empty($this->input->post()))
			{
			$post = $this->input->post();
			if (!empty($post))
				{

				//  print_r($post);die;

				$this->form_validation->set_rules('ques', 'Email', 'trim|required');
				$this->form_validation->set_rules('q1', 'option', 'trim|required');
				$this->form_validation->set_rules('q2', 'option', 'trim|required');
				$this->form_validation->set_rules('q3', 'option', 'trim|required');
				$this->form_validation->set_rules('q4', 'option', 'trim|required');
				$this->form_validation->set_rules('correct', 'option', 'trim|required');
				if ($this->form_validation->run() == TRUE)
					{
					$data = array(
						's_id' => $id,
						'question' => $this->input->post('ques') ,
						'choice1' => $this->input->post('q1') ,
						'choice2' => $this->input->post('q2') ,
						'choice3' => $this->input->post('q3') ,
						'choice4' => $this->input->post('q4') ,
						'correct' => $this->input->post('correct')
					);
					$rest = $this->Common_model->insert_single('choices', $data);
					if ($rest)
						{
						$sultu['s_id'] = $rest;
						$this->session->set_userdata($sultu);
						return redirect('index.php/Dashboard/survey');
						}
					}
				}
			}
		  else
			{
			$this->load->view('Survey');
			}
		}
	}

?>
