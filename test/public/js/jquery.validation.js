jQuery.validator.addMethod("validEmail", function (value, element)
{
   return this.optional(element) || /^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/i.test(value);
}, "Enter valid email address");

$.validator.addMethod("hello", function(value, element)
{
return this.optional(element) || value.length>=8 && /\d/.test(value) && /[a-z]/i.test(value);
}, "Please enter atleast 8 characters which contains atleast 1 letter, 1 number and 1 special character");

$.validator.addMethod("there", function(value, element)
{
return this.optional(element) || value.length==10 && /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
}, "Please enter a valid number of 10 digits");

$.validator.addMethod("nam", function(value, element)
{
return this.optional(element) || value.match(/^[a-zA-Z ]+$/);
}, "Please enter name consisting characters only");

$.validator.addMethod("code", function(value, element)
{
return this.optional(element) || value.match(/^[0-9]*$/);
}, "Please enter valid postal code");


//^[\\p{L} .'-]+$
//validation for admin change basic information
$("#userSignup").validate({
    errorElement: 'span',
    rules: {
        userType: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true
        },

        employee_id: {
          normalizer: function (value) {
              return $.trim(value);
            },
            required: true,
            code: true,
            minlength: 1,
            maxlength: 10
        },

        userName: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true,
            minlength: 3,
            maxlength: 50,
            nam: true
        },
        userEmail: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true,
            maxlength: 100,
            email: true,
            validEmail: true
        },
        mobileNo: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true,
            there: true
        },
        houseNo: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true
        },
        street: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true
        },
        city: {
            normalizer: function (value) {
                return $.trim(value);
            },
            minlength: 3,
            maxlength: 20,
            required: true,
            nam: true
        },
        state: {
            normalizer: function (value) {
                return $.trim(value);
            },

            required: true,
        },
        country: {
            normalizer: function (value) {
                return $.trim(value);
            },
            required: true
        },
        postalCode: {
            normalizer: function (value) {
                return $.trim(value);
            },
            minlength: 5,
            maxlength: 10,
            required: true,
            code: true
        },
           businessCategory: {
               normalizer: function (value) {
                   return $.trim(value);
               },
              required: true
          },
        userPassword: {
            normalizer: function( value ) {
                                return $.trim( value );
                        },
            required:  true,
            minlength: 6,
            maxlength: 50,
            hello: true
        },
        nPassword: {
            normalizer: function( value ) {
                                return $.trim( value );
                        },
            required:  true,
            minlength: 6,
            maxlength: 50,
            hello: true
        },
        cnfmPassword: {
          normalizer: function( value ) {
                              return $.trim( value );
                      },
          required:  true,
          equalTo:   "#userPassword"
        },
        cnfrmPassword: {
          normalizer: function( value ) {
                              return $.trim( value );
                      },
          required:  true,
          equalTo:   "#nPassword"
        }
    },
    messages: {
        userType: {
            required: "User Type is required",
        },
        employee_id: {
          required: "Employee ID is required",
          code: "Enter numeric ID",
          minlength: "ID is too short",
          maxlength: "ID is too long"
        },
        userName: {
            required: "Name is required",
            minlength: "Name is too short",
            maxlength: "Name is too long",
            nam: "Enter characters only"
        },
        userEmail: {
            required: "Enter email address",
            maxlength: "Email Address is Too Long",
            email: "Enter valid email address",
            validEmail: "Enter valid email address"
        },
        mobileNo: {
            required: "Mobile Number is required"
        },
        houseNo: {
            required: "House Number is required"
        },
        street: {
            required: "Street is required"
        },
        city: {
          minlength: "Enter valid City",
          maxlength: "Enter valid city",
            required: "City is required",
            nam: "Enter valid City"
        },
        state: {
            required: "State is required"
        },
        country: {
            required: "Country is required"
        },
        postalCode: {
          minlength: "Code is too short",
          maxlength: "Code is too long",
            required: "Postal Code is required",
            code: "Enter valid postal code"
        },
           businessCategory: {
              required: "Business Category is required"
            },
        userPassword: {
            required:  "Password is required",
            minlength: "Password is too short",
            maxlength: "Password is too long"
        },
        nPassword: {
            required:  "Password is required",
            minlength: "Password is too short",
            maxlength: "Password is too long"
        },
        cnfmPassword: {
            required:  "Confirm Password is required",
            equalTo:   "Password Mismatch"
        },
        cnfrmPassword: {
            required:  "Confirm Password is required",
            equalTo:   "Password Mismatch"
        }
    }
});
